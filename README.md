This datapack/resourcepack adds more types of berries obtainable from the "berry bush" plants:

| Blueberry | Strawberry|
|--|--|
| ![blueberry](./assets/minecraft/textures/item/sweet_blueberries.png) | ![strawberry](./assets/minecraft/textures/item/sweet_strawberry.png) |

Breaking or harvesting a berry bush will have a 30% chance of giving you a blueberry, strawberry, or normal "sweet berries" item.
