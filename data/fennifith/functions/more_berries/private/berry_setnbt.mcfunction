execute store result score #berry berryType run loot spawn ~ -65 ~ loot fennifith:more_berries/berry_type
<% set entity = '@e[type=item,nbt={Item:{id:"minecraft:sweet_berries"}},distance=..0.2,tag=!berrySeen]' %>
execute if score #berry berryType matches 0 at @s as << entity >> run data modify entity @s Item.tag merge value << items.berry.nbt >>
execute if score #berry berryType matches 1 at @s as << entity >> run data modify entity @s Item.tag merge value << items.blueberry.nbt >>
execute if score #berry berryType matches 2 at @s as << entity >> run data modify entity @s Item.tag merge value << items.strawberry.nbt >>
execute at @s as << entity >> run tag @s add berrySeen
