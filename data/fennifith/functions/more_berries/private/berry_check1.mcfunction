scoreboard players set #valid berryType 1

execute if entity @s[nbt={Item:{tag:{CustomModelData:<< items.berry.model >>}}}] run scoreboard players set #valid berryType 0
execute if entity @s[nbt={Item:{tag:{CustomModelData:<< items.blueberry.model >>}}}] run scoreboard players set #valid berryType 0
execute if entity @s[nbt={Item:{tag:{CustomModelData:<< items.strawberry.model >>}}}] run scoreboard players set #valid berryType 0

execute if score #valid berryType matches 1 run function fennifith:more_berries/private/berry_check2
